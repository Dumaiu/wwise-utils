;;;; -*- lexical-binding: t -*-

(defconst +octave-size+ 12
  "Notes in an octave")

(defconst +middle-c-value+ 60)
(defconst +middle-c-note+ 'C4)
(defconst +middle-c-octave+ (first (read-from-string (substring (format "%s" +middle-c-note+) 1))))

(cl-defun midi:valid-scale-step-p (char)
    (and (>= char ?A) (<= char ?G)))


(cl-defun midi:parse-key (symbol-or-string)
  "Given e.g. \"C4\", return a cons, '(C 4)."
  (let* ((string (upcase (format "%s" symbol-or-string)))
	 (scale-step (elt string 0))
	 (octave (read (substring string (1- (length string)))))
	 (accidental (if (= 3 (length string)) ?# nil)))
;(intern (subseq string 1 2))
    (assert (midi:valid-scale-step-p scale-step))
    (assert (> octave 0))
    (list* (intern (char-to-string scale-step))
	   octave
	   (when accidental (list accidental)))))

(cl-defun midi:note-to-value (name)
  "Given a note in [C..B], returns an int in [0..11]."
  (let ((char (etypecase name
		(character (upcase name))
		((or symbol string) (elt (upcase (format "%s" name)) 0)))))
    (ecase char
      (?C 0)
      (?D 2)
      (?E 4)
      (?F 5)
      (?G 7)
      (?A 9)
      (?B 11))))

(progn
  (assert (= (midi:note-to-value 'C) 0))
  (assert (= (midi:note-to-value ?D) 2))  
  (assert (= (midi:note-to-value "E") 4)))

(cl-defun midi:octave-to-value (octave)
  (* +octave-size+ (- octave +middle-c-octave+)))

(assert (= 0 (midi:octave-to-value 4)))

(cl-defun midi:name-to-value (symbol-or-string)
  "Convert from piano-tuner notation to MIDI note value."
  (let ((parsed (midi:parse-key symbol-or-string)))
    (cl-destructuring-bind (note octave &optional accidental) parsed
      (let* ((note-value (midi:note-to-value note))
	     (octave-value (midi:octave-to-value octave))
	     (base-value (+ +middle-c-value+ note-value octave-value)))
	(cl-ecase accidental
	 (?# (1+ base-value))
	 ((nil) base-value))))))

(progn
  (assert (= 60 (midi:name-to-value "C4")))
  (assert (= 61 (midi:name-to-value "C#4")))
  (assert (= 62 (midi:name-to-value 'D4)))
  (assert (= 72 (midi:name-to-value 'C5))))

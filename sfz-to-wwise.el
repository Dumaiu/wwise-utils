;; TODO: I'll need to figure out whether C4 or C5 is middle C in the VSCO!


(defvar *sfz-wwise-mapping*
  '(
    (:sfz "sample" :wwise "Audio File"
	  :documentation "Path to the .wav file, relative to current directory.")
    (:sfz "pitch_keycenter" :wwise "Property[MIDI tracking root note]"
	  :documentation "The sampled note.")
    (:sfz "lokey" :wwise "Property[Key Range Min]")
    (:sfz "hikey" :wwise "Property[Key Range Max]")
    (:sfz "lovel" :wwise "Property[Velocity Min]")
    (:sfz "hivel" :wwise "Property[Velocity Max]")
    (:sfz "volume" :wwise "Property[Voice Volume]")
    ) "Mapping between .sfz-format and Wwise .tsv-format property names.")

(cl-defun sfz:find-by-key (name &key (key :sfz) (mapping *sfz-wwise-mapping*))
  "Returns the entire cons.  Arg should be a string or symbol."
  (cl-find name mapping
	   :key (lambda (plist) (plist-get plist key))
	   :test #'string-equal))

(cl-defun sfz-control-to-wwise (name)
  "Returns the Wwise equivalent.  Arg should be a string or symbol."
  (let ((res (sfz:find-by-key name)))
    (assert res)
    (plist-get res :wwise)))


(cl-defun sfz:parse-control (string)
  (split-string string "=" t "[ \t]+"))

(cl-defun sfz:tokenize-string (&optional string)
  (let ((string (or string (buffer-substring-no-properties (point-min) (point-max))))
	(strings (split-string string "\n" t  "[ \t]+")))
    strings))

(defconst sfz:+region-header+ "<region>")

(cl-defun sfz:parse-<region> (list-of-strings)
  (assert (string-equal (first list-of-strings) sfz:+region-header+))
  ;; FIXME
  )
